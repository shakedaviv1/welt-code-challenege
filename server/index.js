const express = require('express');
const app = express();

const commentsMock = require('./mocks/comments.json');

app.use(express.static('./dist'));


app.get('/api/user/:userId', function (req, res) {
    const {
        params: {
            userId,
        }
    } = req;

    // Require it for every request so that changes can be made to the object without affecting
    // the basic json, thus keeping stateless and immutable.
    const userMock = require('./mocks/user.json');
    userMock.id = userId;
    res.json(userMock);
});

app.get('/api/user/:userId/comment', function (req, res) {
    res.json(commentsMock);
});

app.listen(3030, function () {
    console.log('Example app listening on port 3030!')
});
/**
 * Performs an http call. Returns a promise that resolves on the response. The resolve type is based
 * on the returned content-type. For example json object for application/json type.
 * TODO: Implement other http methods. Implement other converter types.
 *
 * @param {string} url
 * @param {{method?: string}} options
 * @returns {Promise}
 */
const http = (url, options = {}) => {
    const {
        method = 'GET',
    } = options;

    return new Promise((resolve, reject) => {
        const req = http._createRequest();
        http._attachListeners(req,
            (evt) => http._onLoadHandler(evt, resolve),
            (err) => http._onErrorHandler(err, reject));
        req.open(method, url, true);
        req.send();

    });
};

/**
 * Used to create a new request instance
 * @returns {XMLHttpRequest}
 */
http._createRequest = () => {
    return new XMLHttpRequest();
};

/**
 * Attach required listeners on the request instance
 * @param {XMLHttpRequest | EventTarget} req
 * @param {function} onLoadHandler
 * @param {function} onErrorHandler
 */
http._attachListeners = (req, onLoadHandler, onErrorHandler) => {
    req.addEventListener("load", onLoadHandler);
    req.addEventListener("error", onErrorHandler);
};

/**
 * Return the mime type based on the Content-Type header. returns 'default' if no corresponding mime
 * type is found.
 * TODO: Add more mime types (currently only json is implemented)
 * @param {XMLHttpRequest} res
 * @returns {string}
 */
http._getMimeType = res => {
    const contentType = res.getResponseHeader('Content-Type');
    if (contentType.indexOf('application/json') !== -1) {
        return 'json';
    }

    return 'default';
};

/**
 * A map of converter functions that take a response and converts it to the required data type
 */
http._mimeTypeConvertersMap = {
    json: resText => JSON.parse(resText),
    default: resText => resText,
};

/**
 * Returns a converter function based on the mime type
 * @param {string} type
 * @returns {function}
 */
http._getMimeTypeConverter = (type = 'default') => {
    return http._mimeTypeConvertersMap[type] ||  http._mimeTypeConvertersMap.default;
};

/**
 * Handler for request load end event. Takes the response and converts it to the required data type
 * based on the content-type header.
 * @param {EventTarget} evt
 * @param {function} resolve A Promise.resolve function
 */
http._onLoadHandler = (evt, resolve) => {
    const {
        target: res,
    } = evt;

    const mimeType = http._getMimeType(res);
    const mimeTypeConverterFn = http._getMimeTypeConverter(mimeType);
    resolve(mimeTypeConverterFn(res.response));
};

/**
 *
 * @param {Error} err
 * @param {function} reject
 */
http._onErrorHandler = (err, reject) => {
    reject(err);
};


export default http;
import test from 'ava';
import sinon from 'sinon';
import http from './http';


function XMLHttpRequestMock() {
}
XMLHttpRequestMock.prototype.addEventListener = sinon.stub();
XMLHttpRequestMock.prototype.getResponseHeader = sinon.stub();
XMLHttpRequestMock.prototype.open = sinon.stub();
XMLHttpRequestMock.prototype.send = sinon.stub();

test.before(() => {
    global.XMLHttpRequest = XMLHttpRequestMock;
});

test('createRequest: should return an XMLHttpRequest', (t) => {
    const reqInstance = http._createRequest();
    t.true(reqInstance instanceof XMLHttpRequest);
});

test('attachListeners: should invoke addEventListener twice', (t) => {
    const fnA = () => {};
    const fnB = () => {};
    const req = new XMLHttpRequest();

    http._attachListeners(req, fnA, fnB);
    t.true(req.addEventListener.calledWith('load', fnA));
    t.true(req.addEventListener.calledWith('error', fnB));
});

test('getMimeType: should return json when content type is application/json', (t) => {
    const req = new XMLHttpRequest();
    req.getResponseHeader.returns('application/json');
    t.is(http._getMimeType(req), 'json');
});

test('getMimeType: should return default when content type is not recognised', (t) => {
    const req = new XMLHttpRequest();
    req.getResponseHeader.returns('some-type');
    t.is(http._getMimeType(req), 'default');
});

test('getMimeTypeConverter: should return the correct function based on type', (t) => {
    t.is(http._getMimeTypeConverter('json'), http._mimeTypeConvertersMap.json);
    t.is(http._getMimeTypeConverter('default'), http._mimeTypeConvertersMap.default);
    t.is(http._getMimeTypeConverter(), http._mimeTypeConvertersMap.default);
    t.is(http._getMimeTypeConverter('any-other'), http._mimeTypeConvertersMap.default);
});

test('onLoadHandler: should invoke converter function with the response', (t) => {
    const req = new XMLHttpRequest();
    const mockConverterFn = sinon.stub();
    req.response = 'some response';
    sinon.stub(http, '_getMimeType');
    sinon.stub(http, '_getMimeTypeConverter').returns(mockConverterFn);
    http._onLoadHandler({ target: req }, () => {});
    t.true(mockConverterFn.calledWith(req.response));
    http._getMimeType.restore();
    http._getMimeTypeConverter.restore();
});

test('onLoadHandler: should invoke resolve with the result of converter function', (t) => {
    const req = new XMLHttpRequest();
    const converterOutput = 'some value';
    const mockConverterFn = sinon.stub().returns(converterOutput);
    const mockResolve = sinon.stub();
    sinon.stub(http, '_getMimeType');
    sinon.stub(http, '_getMimeTypeConverter').returns(mockConverterFn);
    http._onLoadHandler({ target: req }, mockResolve);
    t.true(mockResolve.calledWith(converterOutput));
    http._getMimeType.restore();
    http._getMimeTypeConverter.restore();
});

test('onErrorHandler: shold invoke reject with provided error', (t) => {
    const mockErr = {some: 'err'};
    const mockReject = sinon.stub();

    http._onErrorHandler(mockErr, mockReject);
    t.true(mockReject.calledWith(mockErr));
});

test('http: should return a Promise', (t) => {
    const httpOutput = http();
    t.true(httpOutput instanceof Promise);
});

test('http: should create a new http-request', (t) => {
    sinon.spy(http, '_createRequest');
    http();
    t.true(http._createRequest.called);
    http._createRequest.restore();
});

test('http: should attach handlers', (t) => {

    const mockResolve = sinon.stub();
    const mockReject = sinon.stub();
    function MockPromise (fn) {
        fn(mockResolve, mockReject);
    }
    sinon.stub(global, 'Promise').callsFake(MockPromise);

    sinon.spy(http, '_attachListeners');
    sinon.stub(http, '_onLoadHandler');
    sinon.stub(http, '_onErrorHandler');
    http();
    t.true(http._attachListeners.called);
    t.true(http._attachListeners.args[0][0] instanceof XMLHttpRequestMock);
    t.true(http._attachListeners.args[0][1] instanceof Function);
    t.true(http._attachListeners.args[0][2] instanceof Function);

    const onLoad = http._attachListeners.args[0][1];
    const onError = http._attachListeners.args[0][2];

    const mockEvt = {some: 'event'};
    onLoad(mockEvt);
    t.true(http._onLoadHandler.calledWith(mockEvt, mockResolve));
    const mockError = {some: 'error'};
    onError(mockError);
    t.true(http._onErrorHandler.calledWith(mockError, mockReject));

    http._attachListeners.restore();
    http._onLoadHandler.restore();
    http._onErrorHandler.restore();
    global.Promise.restore();
});

test('http: should invoke request.open with proper args', (t) => {
    const mockRequest = new XMLHttpRequest();
    sinon.stub(http, '_createRequest').returns(mockRequest);
    http('some url', { method: 'some method'});
    t.true(mockRequest.open.calledWith('some method', 'some url', true));
    http('some url');
    t.true(mockRequest.open.calledWith('GET', 'some url', true));
    http._createRequest.restore();
});

test('http: should invoke request.send', (t) => {
    const mockRequest = new XMLHttpRequest();
    sinon.stub(http, '_createRequest').returns(mockRequest);
    http();
    t.true(mockRequest.send.called);
});


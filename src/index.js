import RestModel from './classes/rest-model';

window.onload = () => {

    const user = new RestModel({
        apiUrl: '/api',
        name: 'user',
        assets: ['comment'],
    });

    user.get('123', true)
        .then(user => {
            const app = document.getElementById('app');
            app.innerHTML = (`<pre>${JSON.stringify(user, null, 2)}</pre>`);
        })
        .catch(err => {
            console.error(err);
        })
};
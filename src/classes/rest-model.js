import http from '../utils/http';
class RestModel {

    /**
     *
     * @param {string} apiUrl
     * @param {string} name
     * @param {Array<string>?} assets
     */
    constructor({ apiUrl, name, assets = [] }) {
        this._apiUrl = apiUrl;
        this._name = name;
        this._assets = assets;
    }

    /**
     * Returns the url to be used in the fetch process <apiUrl>/<name>[/<id>]
     * @param {string?} id
     * @returns {string}
     * @private
     */
    _getUrl(id) {
        const idUrl = id ? `/${id}` : '';
        return `${this._apiUrl}/${this._name}${idUrl}`
    }

    /**
     * Returns a promise that resolves on the model
     * @param {string?}id
     * @returns {Promise<Object|Array<Object>>}
     * @private
     */
    _fetchModel(id) {
        return http(this._getUrl(id));
    }

    /**
     * If assets list was provided, this method will fetch the assets and mount them on the model
     * @param {string} id One place where id is not optional.
     * @returns {Promise.<Array<{name: string, data: Object}>>}
     * @private
     */
    _fetchAssets(id) {
        // Create a list of RestModel from the provided assets
        const assetModels = this._assets.map(assetName => {
            return new RestModel({apiUrl: this._getUrl(id), name: assetName});
        });

        // Create a list of promises where each promise resolve on a fetched asset
        const promises = assetModels.map(assetModel => {
            return assetModel.get()
                .then(model => {
                    // This schema is required to properly mount the assets on the model
                    return {
                        name: assetModel._name,
                        data: model,
                    };
                })
        });
        return Promise.all(promises);
    }

    /**
     * Takes a list of fetched assets and mounts them on the model
     * @param {Object} model
     * @param {Array<{name: string, data: Object}>} assets
     * @returns {Object}
     * @private
     */
    _mountAssetsOnModel(model, assets) {
        assets.forEach(({name, data}) => {
            model[name] = data;
        });
        return model;
    }


    /**
     * Returns a single model or a list of models depending if id was provided. With id an object
     * is returned. Without id a list of objects is returned.
     * @param {string | null?} id
     * @param {boolean?} shouldPopulateAssets
     * @returns {Promise.<Object|Array<Object>>}
     */
    get(id = null, shouldPopulateAssets = false) {

        if (id === null && shouldPopulateAssets) {
            const err = new RangeError('Can not populate assets if model has no id.');
            return Promise.reject(err);
        }

        return this._fetchModel(id)
            .then(model => {
                if (!shouldPopulateAssets) {
                    return model;
                }

                // if assets should be mounted then assets are fetched and mounted
                return this._fetchAssets(id)
                    .then(assets => {
                        return this._mountAssetsOnModel(model, assets);
                    })
            });
    }
}

export default RestModel;

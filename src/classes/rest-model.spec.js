import test from 'ava';
import sinon from 'sinon';

const httpMock = sinon.stub();
const proxyquire = require('proxyquire').noCallThru();

const RestModelModule = proxyquire('./rest-model', {
    '../utils/http': httpMock
});

const {
    'default': RestModel,
} = RestModelModule;

test('instance should have required properties', (t) => {
    const restModel = new RestModel({
        apiUrl: 'some Url',
        name: 'some name',
        assets: ['some assets']
    });
    t.is(restModel._apiUrl, 'some Url');
    t.is(restModel._name, 'some name');
    t.deepEqual(restModel._assets, ['some assets']);

    const restModel2 = new RestModel({
        apiUrl: 'some Url',
        name: 'some name',
    });
    t.is(restModel2._apiUrl, 'some Url');
    t.is(restModel2._name, 'some name');
    t.deepEqual(restModel2._assets, []);
});

test('getUrl: should return ${apiUrl}/${name}[/${id}]', (t) => {
    const restModelConfig = {
        apiUrl: '/api',
        name: 'user',
    };
    const restModel = new RestModel(restModelConfig);

    t.is(restModel._getUrl(), '/api/user');
    t.is(restModel._getUrl('123'), '/api/user/123');
});

test('fetchModel: should invoke http with the output of getUrl', t => {
    const restModelConfig = {
        apiUrl: '/api',
        name: 'user',
    };
    const restModel = new RestModel(restModelConfig);
    sinon.stub(restModel, '_getUrl').returns('some url');
    restModel._fetchModel('some id');
    t.true(restModel._getUrl.calledWith('some id'));
    t.true(httpMock.calledWith('some url'));
});

test('fetchAssets: should return a promise', (t) => {
    const restModelConfig = {
        apiUrl: '/api',
        name: 'user',
    };
    const restModel = new RestModel(restModelConfig);
    const output = restModel._fetchAssets('some id');
    t.true(output instanceof Promise);
});

test('fetchAssets: promises should resolve to a list of objects', async(t) => {
    const restModelConfig = {
        apiUrl: '/api',
        name: 'user',
        assets: ['asset1', 'asset2'],
    };

    const restModel = new RestModel(restModelConfig);
    const modelMock = { some: 'model' };
    sinon.stub(RestModel.prototype, 'get').returns(Promise.resolve(modelMock));
    const results = await restModel._fetchAssets('123');

    t.deepEqual(results, [
        {
            name: 'asset1',
            data: modelMock,
        },
        {
            name: 'asset2',
            data: modelMock,
        },
    ]);

    RestModel.prototype.get.restore();
});

test('mountAssetsOnModel: should place the assets on the model', (t) => {
    const mockData = { some: 'data' };
    const mockAssets = [
        {
            name: 'asset1',
            data: mockData,
        },
        {
            name: 'asset2',
            data: mockData,
        },
    ];
    const mockModel = {};
    RestModel.prototype._mountAssetsOnModel(mockModel, mockAssets);

    t.deepEqual(mockModel, {
        asset1: mockData,
        asset2: mockData,
    });
});

test('get: should reject if id is null and shouldPopulateAssets is true', (t) => {
    const restModelConfig = {
        apiUrl: '/api',
        name: 'user',
    };

    const restModel = new RestModel(restModelConfig);
    restModel.get(null, true)
        .catch(err => {
            t.true(err instanceof RangeError);
        })
});


test('get: if shouldPopulateAssets is false then model should be resolved', (t) => {
    const restModelConfig = {
        apiUrl: '/api',
        name: 'user',
    };

    const restModel = new RestModel(restModelConfig);
    const mockModel = {some: 'data'};
    sinon.stub(restModel, '_fetchModel').returns(Promise.resolve(mockModel));
    restModel.get('123')
        .then(model => {
            t.is(model, mockModel);
        })

});

test('get: if shouldPopulateAssets is true then model should be resolved with assets', (t) => {
    const restModelConfig = {
        apiUrl: '/api',
        name: 'user',
    };

    const restModel = new RestModel(restModelConfig);
    const mockModel = {some: 'data'};
    const mockModel2 = {some: 'data', someExtra: 'data'};
    sinon.stub(restModel, '_fetchModel').returns(Promise.resolve(mockModel));
    sinon.stub(restModel, '_fetchAssets').returns(Promise.resolve());
    sinon.stub(restModel, '_mountAssetsOnModel').returns(Promise.resolve(mockModel2));
    restModel.get('123', true)
        .then(model => {
            t.is(model, mockModel2);
        })

});


# README #

This README would normally document whatever steps are necessary to get your application up and running.

## How do I get set up? ##
### Initial setup ###
* Clone the repo
* run ***npm install***
* run ***npm start***
* Open a browser on ***http://localhost:3030/***

### Run after initial setup ###
* Assuming the project has been cloned and npm install was run
* run ***npm start***
* Open a browser on ***http://localhost:3030/***

### How to run tests
* Assuming the project has been cloned and npm install was run
* run ***npm test*** (for one time run)
* run ***npm run tdd*** (for running the test with watch)

### How to develop
* Assuming the project has been cloned and npm install was run
* run ***npm run nodemon*** - to run the server with watch for changes. alternatively run ***npm run server*** - if you don't wont to work on the server.
* run ***npm run develop*** to fire up webpack with a watch.
* hack away